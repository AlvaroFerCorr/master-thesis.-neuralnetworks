import numpy as np
import tensorflow as tf
import itertools as it
from matplotlib import pyplot as plt
import math
tfk = tf.keras
from bayes_opt import BayesianOptimization
from bayes_opt.logger import JSONLogger
from bayes_opt.event import Events
from bayes_opt.util import load_logs
from functions import *
from callbacks import *

traindata = np.load('traindata.npz')
testdata = np.load('testdata.npz')
psi = np.load('psi.npz')['psi']
npsi=int(len(psi))
ntrainpoints=int(len(traindata['points'])/npsi)
ntestpoints=int(len(testdata['points'])/npsi)
realtrain=np.array(traindata['points'])
realtest=np.array(testdata['points'])
trainomega=np.array(traindata['omega'])
trainweight=np.array(traindata['weights'])
testomega=np.array(testdata['omega'])
testweight=np.array(testdata['weights'])

indices=np.arange(len(realtrain))
np.random.shuffle(indices)
realtrain=realtrain[indices]
trainweight=trainweight[indices]
trainomega=trainomega[indices]
x_train=np.concatenate((np.real(realtrain[:,:-1]),np.imag(realtrain[:,:-1]),np.real(realtrain[:,-1].reshape(-1,1))),axis=-1).astype('float64')
y_train=np.concatenate((trainweight.reshape((-1,1)),trainomega.reshape((-1,1))),axis=-1).astype('float64')
x_test=np.concatenate((np.real(realtest[:,:-1]),np.imag(realtest[:,:-1]),np.real(realtest[:,-1].reshape(-1,1))),axis=-1).astype('float64')
y_test=np.concatenate((testweight.reshape((-1,1)),testomega.reshape((-1,1))),axis=-1).astype('float64')

validation_data=(x_test,y_test)
sigmacb=sigma_callback_mono(validation_data,ntestpoints)
kahlercb=kahler_callback_mono(validation_data,ntestpoints)
Riccicb=Ricci_callback_mono(validation_data,ntestpoints)


nlayer = 4
nHidden = 256
act = 1
#lr = 0.0001
#alpha = 0.0
nfold = 3
nEpochs = 30
bSize = 64
def to_hermitian(x):
    t1 = tf.reshape(tf.complex(x, tf.zeros(9, dtype=tf.float32)), (3,3))
    up = tf.linalg.band_part(t1, 0, -1)
    low = tf.linalg.band_part(1j * t1, -1, 0)
    out = up + tf.transpose(up) - tf.linalg.band_part(t1, 0, 0)
    return out + low + tf.math.conj(tf.transpose(low))

to_hermitian_batch = tf.function(lambda y_pred : tf.vectorized_map(to_hermitian, y_pred))
def Loss(y,g):
    g=to_hermitian_batch(g)
    o=y[:,-1]
    rate=y[:,-2]
    x=rate*tf.abs(tf.ones(tf.shape(o))-tf.math.real(tf.linalg.det(g))/o)
    return x



model = tf.keras.Sequential()
model.add(tfk.Input(shape=(11)))
for i in range(nlayer):
    if act == 0:
        model.add(tfk.layers.Dense(nHidden, activation='tanh'))
    else:
        model.add(tfk.layers.Dense(nHidden, activation='relu'))
model.add(tfk.layers.Dense(9))
model.compile(optimizer=tfk.optimizers.Adam(), 
            loss=Loss)
model.summary()
history = model.fit(x_train, y_train, epochs=nEpochs, batch_size=bSize,
                validation_data=(x_test, y_test), verbose=1, callbacks=[sigmacb,kahlercb,Riccicb])

model.save('model3')
np.save('experiment_ratios.npy',history.history)
