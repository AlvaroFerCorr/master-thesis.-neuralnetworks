import numpy as np
import tensorflow as tf
import itertools as it
from matplotlib import pyplot as plt
import math
from itertools import product
from itertools import permutations

#Function for neural networks, transforming an array of 9 points to an hermitian 3x3 matrix.
def to_hermitian(x):
    t1 = tf.reshape(tf.complex(x, tf.zeros(9, dtype=tf.float32)), (3,3))
    up = tf.linalg.band_part(t1, 0, -1)
    low = tf.linalg.band_part(1j * t1, -1, 0)
    out = up + tf.transpose(up) - tf.linalg.band_part(t1, 0, 0)
    return out + low + tf.math.conj(tf.transpose(low))

to_hermitian_batch = tf.function(lambda y_pred : tf.vectorized_map(to_hermitian, y_pred))

#Combinatory number of n over r
def nCr(n,r):
    f = math.factorial
    return f(n) / f(r) / f(n-r)

#Creates npoints in the complex projective space of dimension=dim
def points_projective_dim(npoints,dim):
    hypercube=np.random.uniform(low=-1,high=1,size=(npoints,2*(dim+1)))
    hypersphere= hypercube/np.linalg.norm(hypercube,axis=-1)[:,None]
    #Transform from hypersphere to projectivespace
    projectivspace=hypersphere[:,:(dim+1)]+1j*hypersphere[:,(dim+1):]
    return projectivspace


#Function that gives npoints of the fermat quintic z0^5+z1^5+...=0 by intersecting a line with the complex projective space
#That is, solving (q0+po*t)^5+...=0 for t and for q and p being points of the complex projective space
def quintic_points(npoints, ndim=4):
    q=points_projective_dim(npoints,ndim)
    p=points_projective_dim(npoints,ndim)
    coeff=np.zeros((np.shape(q)[0],6) , dtype=np.complex128)
    for i in range(6):
        j=5-i
        coeff[:,i]=nCr(5,i)*np.sum(np.multiply(np.power(p,i),np.power(q,j)),axis=-1)
    t=np.zeros((np.shape(q)[0],5), dtype=np.complex128)
    for j in range(np.shape(coeff)[0]):
        t[j]=np.roots(coeff[j,:])
    c=np.einsum("ij,il->ijl",q,t)
    a=np.einsum('ijl-> jli',np.tile(p,(5,1,1)))
    points=c+a
    #points=np.concatenate((points[:,:,0],points[:,:,1],points[:,:,2],points[:,:,3],points[:,:,4]))
    return points






#Function that gives npoints of the fermat quintic z0^5+z1^5+...+z0*z1*...=0 by intersecting a line with the complex projective space
#That is, solving (q0+po*t)^5+...+(q0+p0*t)*...=0 for t and for q and p being points of the complex projective space


def quintic_monomial_points(npoints, phi, ndim=4):
    q=points_projective_dim(npoints,ndim)
    p=points_projective_dim(npoints,ndim)
    coeff=np.zeros((np.shape(q)[0],6) , dtype=np.complex128)
    a=[]
    for i in product([0,1],repeat=5):
        a=a+list(i)
    a=np.reshape(a,(-1,5))
    b=np.count_nonzero(a,axis=-1)

    for i in range(6):
        k=5-i
        #Create all combinations in a list
        maskp= b == i
        Truemaskp=(a[maskp])
        Truemaskq=np.ones(np.shape(Truemaskp))-Truemaskp
        productory=0
        for j in range(len(Truemaskp)):
            productory+=np.prod(np.multiply(np.power(q,Truemaskq[j]),np.power(p,Truemaskp[j])),axis=-1)
        coeff[:,i]=nCr(5,i)*np.sum(np.multiply(np.power(p,i),np.power(q,k)),axis=-1)+phi*productory
    t=np.zeros((np.shape(q)[0],5), dtype=np.complex128)
    for l in range(np.shape(coeff)[0]):
        t[l]=np.roots(coeff[l,:])
    c=np.einsum("ij,il->ijl",q,t)
    a=np.einsum('ijl-> jli',np.tile(p,(5,1,1)))
    points=c+a
    #points=np.concatenate((points[:,:,0],points[:,:,1],points[:,:,2],points[:,:,3],points[:,:,4]))
    return points

#Goes to affine patch from CY threefold by setting the biggest value of a coordinate to be =1

def to_affine(z):
    affine=np.divide(z,z[np.arange(len(z)),np.argmax(np.abs(z),axis=-1)].reshape(-1,1))
    maskaff=~np.isclose(affine, np.ones(np.shape(affine)))
    affine=affine[maskaff].reshape((-1,4))
    return (affine,maskaff)

#Goes to goodcoords by omiting the coordinate with the biggest Jacobian value as to be solved 
#by using the defining equation of the CY threefold

def to_goodcoords(z):
    (affine,maskaff)=to_affine(z)
    Jacobian=np.abs(np.power(affine,4))
    maskgood = Jacobian.max(axis=-1, keepdims=1) ==Jacobian
    goodcoords=affine[~maskgood].reshape((-1,3))
    z1=affine[maskgood].reshape((-1,1))
    return (goodcoords,z1,maskgood, maskaff)



#Gives the value of the holomorphic Volume form for the Fermat Quintic, which is 1/(5*missingz^4)

def Omega(z):
    (goodcoords,missingz,maskgood,maskaff)=to_goodcoords(z)
    return 1/(5*np.power(missingz,4))

#We need to modify the to_goodcoords function in case we want to work with the monomial, as the Jacobian is modified
#by the \prod of the rest of the points (not including the one we are looking at) 

def to_goodcoords_monomial(z,psi):
    (affine,maskaff)=to_affine(z)
    a=[[0, 1, 1, 1,], [1, 0, 1, 1,], [1, 1, 1, 0], [1, 1, 1, 0]]
    Jacobian=np.zeros(np.shape(affine),dtype=np.complex128)
    for i in range (np.shape(a)[0]):
        Jacobian[:,i]=np.abs(np.power(affine[:,i],4)+psi*np.prod(np.power(affine,a[i]),axis=-1))
    maskgood = Jacobian.max(axis=-1, keepdims=1) ==Jacobian
    goodcoords=affine[~maskgood].reshape((-1,3))
    z1=affine[maskgood].reshape((-1,1))
    return (goodcoords,z1,maskgood, maskaff)


#And again, the volume form is modified by the productory of the goodcoords (as one coordinate z=1
#and the missingz is the variable we are derivating with respect to)

def Omega_psi(z,psi):
    (goodcoords,missingz,maskgood,maskaff)=to_goodcoords(z)
    return 1/(5*np.power(missingz,4)+psi*np.prod(goodcoords,axis=-1).reshape((-1,1)))



#Fubini Studi Metric

def FSmetric(z):
    zhat=np.conjugate(z)
    z2=np.sum(zhat*z)  
    n=len(np.abs(z))
    factor=(z2)
    g=1/(np.pi*factor**2) * (factor*np.eye(n)-np.outer(zhat,z))
    return g

#Variation for many points

def FSmetricmanypoints(z):
    zhat=np.conjugate(z)
    z2=np.sum(np.multiply(z,zhat),axis=-1)  
    n=np.shape(z)[1]
    g=np.zeros((np.shape(z)[0],n,n),dtype=np.complex128)
    for i in range(np.shape(g)[0]):
        g[i]=1/(np.pi*z2[i]**2) * (z2[i]*np.eye(n)-np.outer(zhat[i,:],z[i,:]))
    return g


def FSmetric_tf(z):
    zhat=tf.math.conj(z)
    z2=tf.math.reduce_sum(tf.math.multiply(z,zhat),axis=-1)
    n=np.shape(z)[1]
    g= tf.einsum('l,lij-> lij',1/(np.pi*tf.math.pow(z2,2)) , (tf.einsum('l,ij->lij',z2,tf.eye(n,dtype=tf.complex128))  - tf.einsum('lj,li->lij',z,zhat)))
    return g

#Finds the Jacobian \partial z/ \partial x with respect to the change to good coordinates  
#The Jacobian should look like: 1 column of zeros, a 3x3 block of identity and 1 column of -z_i^4/missingz^4

def JacobianMatrixFermat(z):
    (goodcords,missingz,maskgood,maskaff)=to_goodcoords(z)
    Jacobian=np.zeros((np.shape(z)[0],5,3),dtype=np.complex128)
    ChangeJacobian=Jacobian[maskaff,:].reshape((-1,4,3))
    Identitypart=np.array([np.eye(3),]*np.shape(z)[0])
    Derivatedpart=-np.divide(np.power(goodcords,4),np.power(missingz,4))
    ChangeJacobian[np.where(maskgood)]=Derivatedpart
    ChangeJacobian[~maskgood]=Identitypart.reshape(-1,3)
    Jacobian[maskaff,:]=ChangeJacobian.reshape(-1,3)
    Jacobian=np.transpose(Jacobian,(0,2,1))
    return Jacobian

#Computes the weights for each point of the FermatQuintic. It uses the holomorphic volume form, the Jacobian and conjugated Jacobian
#matrices of the change to good coordinates and as initial weights, the Fubini Study metric.

def weightsFermat(z):
    J=JacobianMatrixFermat(z)
    Jhat=np.conjugate(J)
    gFS=FSmetricmanypoints(z)
    pullback=np.einsum('lai,lij,lbj->lab',J,gFS,Jhat)
    volform=Omega(z)*np.conj(Omega(z))
    weights=np.zeros(np.shape(z)[0])
    for i in range (np.shape(weights)[0]):
        weights[i]=np.float(np.real(volform[i]/np.linalg.det(pullback[i,:,:])))
    return weights

#Again, we need to modify the functions in order to have the correct result

#First, we modify the Jacobian matrix of the pullback by modifying the value of \partial z_i / \partial x_a.
#The expression we use for the derivatives is in FermatPointGenerator.ipynb

def JacobianMatrixMono(z,psi):
    (goodcords,missingz,maskgood,maskaff)=to_goodcoords_monomial(z,psi)
    Jacobian=np.zeros((np.shape(z)[0],5,3),dtype=np.complex128)
    ChangeJacobian=Jacobian[maskaff,:].reshape((-1,4,3))
    Identitypart=np.array([np.eye(3),]*np.shape(z)[0])
    a=[[0,1,1],[1,0,1],[1,1,0]]
    productory=np.zeros(np.shape(goodcords),dtype=np.complex128)
    for i in range (np.shape(a)[0]):
        productory[:,i]=np.multiply(missingz,np.prod(np.power(goodcords,a[i]),axis=-1).reshape((-1,1))).reshape(-1)
    Derivatedpart=np.divide(-5*np.power(goodcords,4)-psi*productory,5*np.power(missingz,4)+psi*np.prod(goodcords,axis=-1).reshape(-1,1))
    ChangeJacobian[np.where(maskgood)]=Derivatedpart
    ChangeJacobian[~maskgood]=Identitypart.reshape(-1,3)
    Jacobian[maskaff,:]=ChangeJacobian.reshape(-1,3)
    Jacobian=np.transpose(Jacobian,(0,2,1))
    return Jacobian

#Once the Jacobian matrix is modified, we only need to introduce it in our weight calculator

def weightsMono(z,psi):
    J=JacobianMatrixMono(z,psi)
    Jhat=np.conjugate(J)
    gFS=FSmetricmanypoints(z)
    pullback=np.einsum('lai,lij,lbj->lab',J,gFS,Jhat)
    volform=Omega_psi(z,psi)*np.conj(Omega_psi(z,psi))
    weights=np.zeros(np.shape(z)[0])
    for i in range (np.shape(z)[0]):
        weights[i]=np.float(np.real(volform[i]/np.linalg.det(pullback[i,:,:])))
    return weights




def VolCY(weights):
    return 1/np.shape(weights)[0]* np.sum(weights)



def VolK(weights,omega,g):
    mult=np.multiply(weights,np.linalg.det(g)).reshape((-1,1))
    return 1/ np.shape(weights)[0] * np.sum(np.divide(mult,  omega) )

