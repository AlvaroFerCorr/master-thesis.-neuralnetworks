import numpy as np
import tensorflow as tf
from functions import *

@tf.function
def kahler_loss(model,points,pullback):
    x_vars=points
    with tf.GradientTape(persistent=True) as tape:
        tape.watch(x_vars)                                        #Is it necessary to watch variables?
        prediction=to_hermitian_batch(model(x_vars))              #We compute the metric
        prediciton_real=tf.math.real(prediction)                  #Real part of it   
        prediction_imag=tf.math.imag(prediction)                  #Imag part
    grad_imag=tape.batch_jacobian(prediction_imag,x_vars)         #Perform the Jacobian of both the real and imag part
    grad_real=tape.batch_jacobian(prediciton_real,x_vars)
    grad=tf.cast(grad_real,tf.complex64)                                                                                             +1j*tf.cast(grad_imag,tf.complex64)                           #Combine Jacobians to get the Jacobian with real and imag part

    truegrad=0.5*grad[:,:,:,:5]-0.5*1j*grad[:,:,:,5:-1]           #Combine the first 5 real derivatives with the other 5                                                                           imaginary part derivative to get the Jacobian in ambient                                                                         space coordinates
    pullback=tf.cast(pullback,tf.complex64)
    g_lijk=tf.einsum('lkm,lijm->lijk',pullback,truegrad)          #Above, we apply the pullback to goodcoordinates by                                                                              multiplying the derivative of the metric in ambient space                                                                        coordinates by the jacobian of change of coordinates to                                                                          goodcoordinates
    c_lijk=g_lijk-tf.einsum("lijk->lkji",g_lijk)                  #c_lijk is defined as g_lijk-g_lkji 
    loss_kahler=tf.reduce_sum(tf.math.abs(tf.math.real((c_lijk))),
    axis=(-1,-2,-3))+tf.reduce_sum(tf.math.abs(tf.math.imag((c_lijk))),                                                          
    axis=(-1,-2,-3))                                              #Last, the kahler loss is the sum of all real+imag parts of                                                                      all coordinates
    return loss_kahler


def VolCY(weights):
    return 1/np.shape(weights)[0]* np.sum(weights)
def VolK(weights,omega,g):
    mult=np.multiply(weights,np.linalg.det(g)).reshape((-1,1))
    return 1/ np.shape(weights)[0] * np.sum(np.divide(mult,  omega) )
def sigma_measure(weights,omega,g):
    VolCalabi=VolCY(weights)
    VolKa=VolK(weights,omega,g)
    integrand=np.abs(np.ones(np.shape(omega))- np.divide(np.linalg.det(g).reshape(-1,1)/VolKa  , (omega/VolCalabi)))
    return 1/(np.shape(weights)[0]*VolCalabi)*np.sum(np.multiply(integrand,weights.reshape(-1,1)))



def Ricci_scalar(model,points,pullback):
    x_var=tf.Variable(points)
    with tf.GradientTape(persistent=True) as tape:
        tape.watch(x_var)
        with tf.GradientTape(persistent=True) as tape2:
            tape2.watch(x_var)
            prediction=to_hermitian_batch(model(x_var))
            lndet=tf.reshape(tf.math.log(tf.math.real(tf.linalg.det(prediction))),(-1,1))
        dR_dz=tf.reshape(tape2.batch_jacobian(lndet,x_var),(-1,np.shape(points)[1]))
    d2R_dzdz=tape.batch_jacobian(dR_dz,x_var)
    d2R_dzdz=d2R_dzdz.numpy()
    Ricci_tensor=0.5*d2R_dzdz[:,:,:5]+0.5*1j*d2R_dzdz[:,:,5:-1]
    Ricci_tensor=0.5*Ricci_tensor[:,:5,:]-0.5*1j*Ricci_tensor[:,5:-1,:]
    Ricci_tensor=np.einsum('lai,lbj,lij->lab',pullback, np.conjugate(pullback),Ricci_tensor)
    Ricci_scalar=np.einsum('lij,lij->l', tf.linalg.inv(prediction).numpy(), Ricci_tensor  )
    return Ricci_scalar

def Ricci_measure(model,points,pullback,weights,omega):
    g=to_hermitian_batch(model(points))
    volK=VolK(weights,omega,g)
    volCY=VolCY(weights)
    Ricci=np.abs(Ricci_scalar(model,points,pullback).reshape((-1,1)))
    to_sum=np.linalg.det(g).reshape((-1,1))/omega * np.multiply(Ricci,weights.reshape((-1,1)))
    measure=np.power(volK,1/3)/volCY * np.mean(to_sum,dtype=np.float64)
    return measure


class sigma_callback(tf.keras.callbacks.Callback):
    def __init__(self, validation_data):
        r"""A callback which computes the sigma measure for
        the validation data after every epoch end.

        Args:
            validation_data (tuple(X_val, y_val)): validation data
        """
        super(sigma_callback, self).__init__()
        self.X_val, self.y_val = validation_data
        self.weights = tf.cast(self.y_val[:,-2], tf.float32)
        self.omega = tf.cast(self.y_val[:,-1], tf.float32)

    def on_epoch_end(self, epoch, logs=None):
        prediction=to_hermitian_batch(self.model(self.X_val)).numpy()
        weights=self.weights.numpy()
        omega=self.omega.numpy().reshape(-1,1)
        sigma=sigma_measure(weights,omega,prediction)
        logs['sigma_val'] = sigma.tolist()
        print('\n Sigma measure val: {}.'.format(sigma))




class kahler_callback(tf.keras.callbacks.Callback):
    def __init__(self, validation_data):
        r"""A callback which computes the sigma measure for
        the validation data after every epoch end.

        Args:
            validation_data (tuple(X_val, y_val)): validation data
        """
        super(kahler_callback, self).__init__()
        self.X_val, self.y_val = validation_data
        self.pullback=JacobianMatrixFermat(self.X_val[:,:5]+1j*self.X_val[:,5:])
        #self.weights = tf.cast(self.y_val[:,-2], tf.float32)
        #self.omega = tf.cast(self.y_val[:,-1], tf.float32)

    def on_epoch_end(self, epoch, logs=None):
        model=self.model
        
        #weights=self.weights.numpy()
        #omega=self.omega.numpy().reshape(-1,1)
        kahler=np.mean(kahler_loss(model,self.X_val,self.pullback).numpy())
        logs['kahler_val'] = kahler.tolist()
        print('\n Kahler loss val: {}.'.format(kahler))



class Ricci_callback(tf.keras.callbacks.Callback):
    def __init__(self, validation_data):
        r"""A callback which computes the sigma measure for
        the validation data after every epoch end.

        Args:
            validation_data (tuple(X_val, y_val)): validation data
        """
        super(Ricci_callback, self).__init__()
        self.X_val, self.y_val = validation_data
        self.trainpoints=self.X_val[:,:5]+1j*self.X_val[:,5:]
        self.pullback=JacobianMatrixFermat(self.trainpoints)
        self.weights = self.y_val[:,-2]
        self.omega = self.y_val[:,-1].reshape((-1,1))

    def on_epoch_end(self, epoch, logs=None):
        model=self.model
        
  #      weights=self.weights.numpy()
        omega=self.omega.reshape(-1,1)
        Ricci=Ricci_measure(model,self.X_val,self.pullback,self.weights,self.omega)
        logs['ricci_val'] = Ricci.tolist()
        print('\n Ricci measure val: {}.'.format(Ricci))


class sigma_callback_mono(tf.keras.callbacks.Callback):
    def __init__(self, validation_data, split):
        r"""A callback which computes the sigma measure for
        the validation data after every epoch end.

        Args:
            validation_data (tuple(X_val, y_val)): validation data
        """
        super(sigma_callback_mono, self).__init__()
        self.X_val, self.y_val = validation_data
        self.psi = self.X_val[:,-1]
        self.points = self.X_val[:,:-1]
        self.weights = tf.cast(self.y_val[:,-2], tf.float32)
        self.omega = tf.cast(self.y_val[:,-1], tf.float32)
        self.NumLoop= np.shape(self.X_val)[0]/split
        self.split= split
    def on_epoch_end(self, epoch, logs=None):
        prediction=to_hermitian_batch(self.model(self.X_val)).numpy()
        weights=self.weights.numpy()
        omega=self.omega.numpy().reshape(-1,1)
        for i in range(int(self.NumLoop)):
            sigma=sigma_measure(weights[i*self.split:(i+1)*self.split],omega[i*self.split:(i+1)*self.split],prediction[i*self.split:(i+1)*self.split])
            logs[f'sigma_val_psi{self.psi[i*self.split]}'] = sigma.tolist()
        print('\n Sigma measure val: {}.'.format(sigma))




class kahler_callback_mono(tf.keras.callbacks.Callback):
    def __init__(self, validation_data, split):
        r"""A callback which computes the sigma measure for
        the validation data after every epoch end.

        Args:
            validation_data (tuple(X_val, y_val)): validation data
        """
        super(kahler_callback_mono, self).__init__()
        self.X_val, self.y_val = validation_data
        self.psi = self.X_val[:,-1]
        self.points = self.X_val[:,:-1]
        self.NumLoop= np.shape(self.X_val)[0]/split
        self.split= split
        self.pullback=[]
        for i in range(int(self.NumLoop)):
            self.pullback+=JacobianMatrixMono(self.points[i*split:(i+1)*split,:5]+1j*self.points[i*split:(i+1)*split,5:],self.psi[i*split]).tolist()
        self.pullback=np.array(self.pullback)
        #self.weights = tf.cast(self.y_val[:,-2], tf.float32)
        #self.omega = tf.cast(self.y_val[:,-1], tf.float32)

    def on_epoch_end(self, epoch, logs=None):
        model=self.model
        
        #weights=self.weights.numpy()
        #omega=self.omega.numpy().reshape(-1,1)
        for i in range(int(self.NumLoop)):
            kahler=np.mean(kahler_loss(model,self.X_val[i*self.split:(i+1)*self.split],self.pullback[i*self.split:(i+1)*self.split]).numpy())
            logs[f'kahler_val_psi{self.psi[i*self.split]}'] = kahler.tolist()
        print('\n Kahler loss val: {}.'.format(kahler))



class Ricci_callback_mono(tf.keras.callbacks.Callback):
    def __init__(self, validation_data, split):
        r"""A callback which computes the sigma measure for
        the validation data after every epoch end.

        Args:
            validation_data (tuple(X_val, y_val)): validation data
        """
        super(Ricci_callback_mono, self).__init__()
        self.X_val, self.y_val = validation_data
        self.psi = self.X_val[:,-1]
        self.points = self.X_val[:,:-1]
        self.NumLoop= np.shape(self.X_val)[0]/split
        self.split= split
        self.pullback=[]
        for i in range(int(self.NumLoop)):
            self.pullback+=JacobianMatrixMono(self.points[i*split:(i+1)*split,:5]+1j*self.points[i*split:(i+1)*split,5:],self.psi[i*split]).tolist()
        self.pullback=np.array(self.pullback)
        self.weights = self.y_val[:,-2]
        self.omega = self.y_val[:,-1].reshape((-1,1))

    def on_epoch_end(self, epoch, logs=None):
        model=self.model
        
  #      weights=self.weights.numpy()
        omega=self.omega.reshape(-1,1)
        for i in range(int(self.NumLoop)):
            Ricci=Ricci_measure(model,self.X_val[i*self.split:(i+1)*self.split],self.pullback[i*self.split:(i+1)*self.split],self.weights[i*self.split:(i+1)*self.split],self.omega[i*self.split:(i+1)*self.split])
            logs[f'ricci_val_psi{self.psi[i*self.split]}'] = Ricci.tolist()
        print('\n Ricci measure val: {}.'.format(Ricci))