import numpy as np
import tensorflow as tf
import itertools as it
from matplotlib import pyplot as plt
import math
tfk = tf.keras
from bayes_opt import BayesianOptimization
from bayes_opt.logger import JSONLogger
from bayes_opt.event import Events
from bayes_opt.util import load_logs


traindata=np.load('train.npy')
trainpoints=traindata['point']
testdata=np.load('test.npy')
newtrain=np.concatenate([np.real(trainpoints),np.imag(trainpoints)],axis=-1)
omega=traindata['omega']

def to_hermitian(x):
    t1 = tf.reshape(tf.complex(x, tf.zeros(9, dtype=tf.float32)), (3,3))
    up = tf.linalg.band_part(t1, 0, -1)
    low = tf.linalg.band_part(1j * t1, -1, 0)
    out = up + tf.transpose(up) - tf.linalg.band_part(t1, 0, 0)
    return out + low + tf.math.conj(tf.transpose(low))

to_hermitian_batch = tf.function(lambda y_pred : tf.vectorized_map(to_hermitian, y_pred))

def Loss(y,g):
    g=to_hermitian_batch(g)
    x=tf.abs(tf.ones(tf.shape(y))-tf.math.real(tf.linalg.det(g))/y)
    return x

def make_model(nHidden, nlayer,act):
    model=tfk.Sequential()
    model.add(tfk.layers.Input(shape=(10)))
    for i in range(nlayer):
        if act==0:
            model.add(tfk.layers.Dense(nHidden,activation='relu'))
        elif act==1:
            model.add(tfk.layers.Dense(nHidden,activation='tanh'))
        else:
            model.add(tfk.layers.Dense(nHidden,activation='sigmoid'))
    model.add(tfk.layers.Dense(9))
    model.compile(optimizer='Adam',loss=Loss)
    return model


def get_loss(nHidden, nlayer, act, batchSize=128, vsplit=0.4, nEpochs=8):
    m=make_model(nHidden, nlayer,act)
    history=m.fit(newtrain, omega, epochs=nEpochs, batch_size=batchSize, validation_split=vsplit)
    loss = history.history['val_loss'][-1]
    return loss


def f(nHidden, nlayer, batchSize, act, nEpochs=8):
    int_hidden=int(round(nHidden))
    int_nlayer=int(round(nlayer))
    int_batch=int(round(batchSize))
    int_epochs=int(round(nEpochs))
    int_act=int(np.floor(act))
    loss=get_loss(nHidden=int_hidden,nlayer=int_nlayer, act=int_act,batchSize=int_batch,)
    return 1-loss

new_optimizer = BayesianOptimization(
    f = f,
    pbounds={"nHidden": (128, 256), "nlayer": (3,6), "batchSize": (128,256),"act" : (0,3)},
    verbose=2,
    random_state=7,
)


logger = JSONLogger(path="./logs.json2")
new_optimizer.subscribe(Events.OPTIMIZATION_STEP, logger)
load_logs(new_optimizer, logs=["./logs.json"])
new_optimizer.maximize(init_points=45,n_iter=2000)
